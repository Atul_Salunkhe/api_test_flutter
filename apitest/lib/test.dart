import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Test extends StatefulWidget {
  const Test({super.key});

  @override
  State<Test> createState() => _TestState();
}

class _TestState extends State<Test> {
  List<String> makeCodeValues = [];
  String? selectedDropdownValue;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDatafromServer();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Api task'),
        centerTitle: true,
        backgroundColor: Colors.deepPurple,
      ),
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(
              height: 20,
            ),
            const Text('  Select firm :- '),
            const SizedBox(
              height: 20,
            ),
            Container(
              margin: const EdgeInsets.only(right: 10, left: 10),
              padding: const EdgeInsets.only(left: 5, right: 5),
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.deepPurple,
                  width: 2,
                ),
                borderRadius: BorderRadius.circular(8),
              ),
              child: DropdownButton<String>(
                borderRadius: BorderRadius.circular(10),
                isExpanded: true,
                value: selectedDropdownValue,
                items: makeCodeValues.map((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Center(
                      child: Text(
                        value,
                      ),
                    ),
                  );
                }).toList(),
                onChanged: (String? newValue) {
                  setState(() {
                    selectedDropdownValue = newValue;
                  });
                },
                hint: const Center(
                  child: Text(
                    'Select firm',
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> getDatafromServer() async {
    try {
      Map<String, dynamic> requestData = {
        "api_request_type": "MOB_APP",
        "ecom_own_id": "101010",
        "ecom_login_id": "lrearth",
        "system_onoff": "ON",
        "GB_DB_HOST": "43.205.122.41",
        "user_login_id": "lrearth",
        "ecom_domain_name": "omunim.com",
        "ecom_api_key": "",
        "api_login_token": "abc12313",
        "api_prod_key": "123123",
        "api_request_id": "lrearth",
        "api_folder": "",
        "mapi_folder": "",
        "remote_login": "HTTP_REMOTE_LOGIN",
        "GB_DB_PORT": "3306",
        "GB_DB_USER": "",
        "USER_DB_HOST": "",
        "USER_DB_PORT": "",
        "USER_DB_USER": "",
        "owner_login_id": "lrearth",
        "owner_user_password": "",
        "utin_id": 296,
        "utin_user_id": 10471,
        "utin_transaction_type": "ADV MONEY",
        "function_name": "get_transaction"
      };
      var response = await http.post(
        Uri.parse('https://mapi.omunim.in/transaction/get_crdr_details'),
        body: jsonEncode(requestData),
        headers: {'Content-Type': 'application/json'},
      );

      if (response.statusCode == 200) {
        List<dynamic> responseData = jsonDecode(response.body);

        setState(() {
          makeCodeValues = responseData
              .map<String>((map) => map['utin_firm_id'] as String)
              .toList();
        });
      } else {
        throw Exception('Failed to load data: ${response.statusCode}');
      }
    } catch (error) {
      print('Error fetching data: $error');
      throw Exception('Failed to load data: $error');
    }
  }
}
